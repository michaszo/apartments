<?php

/*
|--------------------------------------------------------------------------
| Web Routes
|--------------------------------------------------------------------------
|
| Here is where you can register web routes for your application. These
| routes are loaded by the RouteServiceProvider within a group which
| contains the "web" middleware group. Now create something great!
|
*/

Route::get('/', function () {
    return view('main');
});

Auth::routes();

Route::resource('app', 'AppController');

Route::get('/search', 'SearchController@apartments')
    ->name('search');

Route::middleware(['auth', 'owner'])->group(function () {


    Route::resource('apartments', 'ApartmentsController');

    Route::resource('rooms', 'RoomsController');

    Route::get('rooms/create/{id?}', 'RoomsController@create')
        ->name('rooms.create');

    Route::resource('rent', 'RentController');

    Route::get('rent/create/{id}', 'RentController@create')
        ->name('rent.create');

    Route::resource('note', 'NoteController');

    Route::resource('finance', 'FinanceController');
});

Route::middleware(['auth'])->group(function () {
    Route::resource('home', 'HomeController');
    Route::resource('forum', 'ForumController');
    Route::get('forum/tenant/{id}', 'ForumController@tenant')
        ->name('forum.tenant');
});


