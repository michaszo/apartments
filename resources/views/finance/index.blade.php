@extends('layouts.app')

@section('content')
    <div class="row">
        <div class="col-12">
            <div class="row text-center p-2 pt-4 border flex-md-row mb-4 shadow-sm h-md-250">
                <div class="col-lg-12">
                    <h4 class="mt-1">zysk:</h4>
                    {{--                    zysk = przychód - koszty--}}
                    <h5 class="text-secondary">{{$profit}}PLN</h5>
                </div>
            </div>
            <div class="row text-center bg-light p-2 pt-4 border flex-md-row mb-4 shadow-sm h-md-250">
                <div class="col-lg-6">
                    <h4 class="mt-1">przychód:</h4>
                    <h5 class="text-success">{{$income}}PLN</h5>
                </div>
                <div class="col-lg-6">
                    <h4 class="mt-1">koszty:</h4>
                    <h5 class="text-danger">{{$outgo}}PLN</h5>
                </div>
            </div>
            <div class="row text-center p-2 pt-4 border flex-md-row mb-4 shadow-sm h-md-250">
                <div class="col-lg-6">
                    <h4 class="mt-1">przychód z czynszu:</h4>
                    <h5 class="text-success">{{$rental_profit}}PLN</h5>
                </div>
                <div class="col-lg-6">
                    <h4 class="mt-1">rachunki:</h4>
                    <h5 class="text-danger">{{$fees}}PLN</h5>
                </div>
            </div>
        </div>
    </div>
    <div class="row text-center">
        <div class="col-12">
            <table class="table table-borderless bg-light border flex-md-row shadow-sm h-md-250">
                <thead>
                <tr>
                    <th scope="col">data</th>
                    <th scope="col">mieszkanie</th>
                    <th scope="col">opis</th>
                    <th scope="col">kwota</th>
                </tr>
                </thead>
                <tbody>
                @forelse($finances as $finance)
                <tr class="{{($finance->type === 'income') ? 'table-success' : 'table-danger'}}">
                    <td>{{$finance->created_at}}</td>
                    <td>{{$finance->apartment->city}} <small> {{$finance->apartment->street}} - {{$finance->apartment->apartment_number}} </small> </td>
                    <td>{{$finance->title}}</td>
                    <td><b>{{$finance->amount}}</b>PLN</td>
                </tr>
                    @empty
                    <h2>Brak rozliczeń</h2>
                @endforelse
                </tbody>
            </table>
        </div>
    </div>
@endsection
