@extends('layouts.app')

@section('content')
    <div class="row">
        <div class="col-12">
            @forelse($apartments as $apartment)
                <div
                    class="pl-2 no-gutters border flex-md-row mb-4 shadow-sm h-md-250 position-relative">
                    <div class="row">

                        <div class="col-7 p-4 d-flex flex-column position-static">
                            <strong class="d-inline-block text-primary"><h4>{{$apartment->city}} {{$apartment->street}}</h4></strong>
                            <div class="text-muted small">{{$apartment->created_at}}</div>
                            <h4 class="mt-3">{{$apartment->title}}</h4>
                            <p class="card-text mb-auto">{!!  $apartment->description !!}</p>

                            <div class="btn-group">
                                <button type="button" class="btn btn-primary dropdown-toggle px-1" data-toggle="dropdown" data-display="static" aria-haspopup="true" aria-expanded="false">
                                    menu
                                </button>
                                <div class="dropdown-menu dropdown-menu-right dropdown-menu-lg-left">
                                    <a href="{{route('apartments.show', $apartment->id)}}">
                                        <button type="button" class="btn btn-primary">Finanse</button>
                                    </a>
                                    <a href="{{route('apartments.show', $apartment->id)}}">
                                        <button type="button" class="btn btn-success">Podgląd</button>
                                    </a>
                                    <a href="{{route('apartments.edit', $apartment->id)}}">
                                        <button type="button" class="btn btn-warning">Edytuj</button>
                                    </a>
                                    <a href="{{route('apartments.show', $apartment->id)}}">
                                        <button type="button" class="btn btn-danger">Zawieś</button>
                                    </a>
                                    <a href="{{route('rooms.create', $apartment->id)}}" class="">
                                        <button type="button" class="btn btn-primary">dodaj pokój</button>
                                    </a>
                                </div>
                            </div>
                        </div>
                        <div class="col-5 d-none d-lg-block">
                            <div class="mt-5 p-3">
                                <img src="{{ asset('storage/'. $apartment->img_1)}}" class="img-fluid">
                            </div>
                        </div>
                    </div>
                </div>
            @empty
            @endforelse
        </div>
    </div>
@endsection
