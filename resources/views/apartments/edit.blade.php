@extends('layouts.app')

@section('content')
    <div class="row">
        <div class="col-12">
            <div class="card card-default">
                <div class="card-header">
                    <h5>edytowanie mieszkania</h5>
                </div>
                <div class="card-body">
                    <form action="{{route('apartments.update', $apartment->id)}}" method="post"
                          enctype="multipart/form-data">
                        @csrf
                        @method('PUT')

                        <div class="form-row">
                            <div class="col-md-12 mb-3">
                                <label for="title">Tytuł</label>
                                <input type="text" class="form-control" id="title" name="title"
                                       value="{{old('title') ?? $apartment->title}}">
                            </div>
                        </div>

                        <div class="form-row">
                            <div class="col-md-5 mb-3">
                                <label for="title">Miasto</label>
                                <input type="text" class="form-control" id="city" name="city"
                                       value="{{old('city') ?? $apartment->city}}">
                            </div>
                            <div class="col-md-5 mb-3">
                                <label for="title">Ulica</label>
                                <input type="text" class="form-control" id="street" name="street"
                                       value="{{old('street') ?? $apartment->street}}">
                            </div>
                            <div class="col-md-2 mb-3">
                                <label for="title">Numer budynku</label>
                                <input type="text" class="form-control" id="apartment_number" name="apartment_number"
                                       value="{{old('apartment_number') ?? $apartment->apartment_number}}">
                            </div>
                        </div>

                        <div class="form-row">
                            <div class="col-md-3 mb-3">
                                <label for="title">Ilość miejsc</label>
                                <input type="text" class="form-control" id="number_of_beds" name="number_of_beds"
                                       value="{{old('number_of_beds') ?? $apartment->number_of_beds}}">
                            </div>
                            <div class="col-md-4 offset-1 mb-3">
                                <label for="title">Cena za wynajem</label>
                                <input type="text" class="form-control" id="rental_price" name="rental_price"
                                       value="{{old('rental_price') ?? $apartment->rental_price}}">
                            </div>
                            <div class="col-md-3 offset-1 mb-3">
                                <label for="title">Cena za media</label>
                                <input type="text" class="form-control" id="fees" name="fees"
                                       value="{{old('fees') ?? $apartment->fees}}">
                            </div>
                        </div>

                        <div class="form-row">
                            <div class="col-md-12 mb-3">
                                <label for="title">Opis</label>
                                <input id="description" type="hidden" name="description">
                                <trix-editor
                                    input="description">{!! old('description') ?? $apartment->description !!}</trix-editor>
                            </div>
                        </div>

                        <div>
                            <label for="title">Dodaj zdjęcia</label>

                            <div class="form-row mb-3">
                                <div class="col-md-6">
                                    <div class="input-group mb-3">
                                        <div class="custom-file">
                                            <input type="file" class="custom-file-input" name="img_1" id="img_1">
                                            <label class="custom-file-label" for="img_1">zdjęcie 1</label>
                                        </div>
                                        @if($apartment->img_1)
                                            <div class="mt-5">
                                                <img src="{{ asset('storage/'. $apartment->img_1)}}" class="img-fluid">
                                            </div>
                                        @endif
                                    </div>
                                </div>
                                <div class="col-md-6">
                                    <div class="custom-file">
                                        <input type="file" class="custom-file-input" name="img_2" id="img_2">
                                        <label class="custom-file-label" for="img_2">zdjęcie 2</label>
                                    </div>
                                    @if($apartment->img_2)
                                        <div class="mt-5">
                                            <img src="{{ asset('storage/'. $apartment->img_2)}}" class="img-fluid">
                                        </div>
                                    @endif
                                </div>
                            </div>

                            <div class="form-row mb-3">
                                <div class="col-md-6">
                                    <div class="input-group mb-3">
                                        <div class="custom-file">
                                            <input type="file" class="custom-file-input" name="img_3" id="img_3">
                                            <label class="custom-file-label" for="img_3">zdjęcie 3</label>
                                        </div>
                                        @if($apartment->img_3)
                                            <div class="mt-5">
                                                <img src="{{ asset('storage/'. $apartment->img_3)}}" class="img-fluid">
                                            </div>
                                        @endif
                                    </div>
                                </div>
                                <div class="col-md-6">
                                    <div class="custom-file">
                                        <input type="file" class="custom-file-input" name="img_4" id="img_4">
                                        <label class="custom-file-label" for="img_4">zdjęcie 4</label>
                                    </div>
                                    @if($apartment->img_4)
                                        <div class="mt-5">
                                            <img src="{{ asset('storage/'. $apartment->img_4)}}" class="img-fluid">
                                        </div>
                                    @endif
                                </div>
                            </div>
                        </div>

                        <div class="form-group text-center my-5">
                            <button class="btn btn-success btn-block" type="submit">zapisz zmiany</button>
                        </div>

                    </form>
                </div>
            </div>
        </div>
@endsection

