@extends('layouts.app')

@section('content')
    <div class="row">
        <div class="col-12">
            @include('note.notes')
        </div>
    </div>

    <div class="row mt-5">
        <div class="col-12">
            <div class="card p-3 rounded shadow-sm">
                <div class="card-header">
                    <h6>Numer ogłoszenia: {{$apartment->id}}</h6>
                </div>
                <div class="card-body">
                    <ul class="list-group list-group-flush">
                        <li class="list-group-item">
                            <span>Tytuł:</span>
                            <span class="font-weight-bold">{{$apartment->title}}</span>
                        </li>

                        <li class="list-group-item d-flex justify-content-between">
                            <span>
                                <span>Miasto:</span>
                                <span class="font-weight-bold">{{$apartment->city}}</span>
                            </span>
                            <span>
                                <span>Ulica:</span>
                                <span class="font-weight-bold">{{$apartment->street}}</span>
                            </span>
                            <span>
                                <span>Numer budynku:</span>
                                <span class="font-weight-bold">{{$apartment->apartment_number}}</span>
                            </span>
                        </li>

                        <li class="list-group-item d-flex justify-content-between">
                            <span>
                                <span>Ilość łóżek:</span>
                                <span class="font-weight-bold">{{$apartment->number_of_beds}}</span>
                            </span>
                            <span>
                                <span>Cena wynajmu (za całość):</span>
                                <span class="font-weight-bold">{{$apartment->rental_price}}</span>
                            </span>
                            <span>
                                <span>Opłaty:</span>
                                <span class="font-weight-bold">{{$apartment->fees}}</span>
                            </span>
                        </li>

                        <li class="list-group-item">
                            <span>Opis:</span>
                            <span class="font-weight-bold">{!! $apartment->description !!}</span>
                        </li>

                        <li class="list-group-item">
                            @if($apartment->img_2)
                                <div class="row">
                                    <div class="col-6">
                                        <div class="mt-5">
                                            <img src="{{ asset('storage/'. $apartment->img_1)}}" class="img-fluid">
                                        </div>
                                    </div>

                                    <div class="col-6">
                                        <div class="mt-5">
                                            <img src="{{ asset('storage/'. $apartment->img_2)}}" class="img-fluid">
                                        </div>
                                    </div>
                                </div>

                                <div class="row">
                                    <div class="col-6">
                                        @if($apartment->img_3)
                                            <div class="mt-5">
                                                <img src="{{ asset('storage/'. $apartment->img_3)}}" class="img-fluid">
                                            </div>
                                        @endif
                                    </div>

                                    <div class="col-6">
                                        @if($apartment->img_4)
                                            <div class="mt-5">
                                                <img src="{{ asset('storage/'. $apartment->img_4)}}" class="img-fluid">
                                            </div>
                                        @endif
                                    </div>
                                </div>
                        </li>
                        @else
                            <div class="mt-5">
                                <img src="{{ asset('storage/'. $apartment->img_1)}}" class="img-fluid">
                            </div>
                        @endif

                        <li class="list-group-item">
                            <span>Dodano:</span>
                            <span class="font-weight-bold">{{ $apartment->created_at }}</span>
                        </li>
                    </ul>
                </div>
            </div>
        </div>
    </div>
@endsection

