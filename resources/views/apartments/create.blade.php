@extends('layouts.app')

@section('content')
    <div class="row">
        <div class="col-12">
            <div class="card card-default">
                <div class="card-header">
                    <h5>dodawanie nowego mieszkania</h5>
                </div>
                <div class="card-body">
                    <form action="{{route('apartments.store')}}" method="post" enctype="multipart/form-data">
                        @csrf

                        <div class="form-row">
                            <div class="col-md-12 mb-3">
                                <label for="title">Tytuł</label>
                                <input type="text" class="form-control" id="title" name="title"
                                       value="{{ old('title') }}"
                                       placeholder="Słoneczne, przestronne mieszkanie dla 4 studentów">
                            </div>
                        </div>

                        <div class="form-row">
                            <div class="col-md-5 mb-3">
                                <label for="title">Miasto</label>
                                <input type="text" class="form-control" id="city" name="city"
                                       value="{{ old('city') }}"
                                       placeholder="Warszawa">
                            </div>
                            <div class="col-md-5 mb-3">
                                <label for="title">Ulica</label>
                                <input type="text" class="form-control" id="street" name="street"
                                       value="{{ old('street') }}"
                                       placeholder="Wielkopolska">
                            </div>
                            <div class="col-md-2 mb-3">
                                <label for="title">Numer budynku</label>
                                <input type="text" class="form-control" id="apartment_number" name="apartment_number"
                                       value="{{ old('apartment_number') }}"
                                       placeholder="4b">
                            </div>
                        </div>

                        <div class="form-row">
                            <div class="col-md-3 mb-3">
                                <label for="title">Ilość miejsc</label>
                                <input type="text" class="form-control" id="number_of_beds" name="number_of_beds"
                                       value="{{ old('number_of_beds') }}"
                                       placeholder="5">
                            </div>
                            <div class="col-md-4 offset-1 mb-3">
                                <label for="title">Cena za wynajem</label>
                                <input type="text" class="form-control" id="rental_price" name="rental_price"
                                       value="{{ old('rental_price') }}"
                                       placeholder="1300">
                            </div>
                            <div class="col-md-3 offset-1 mb-3">
                                <label for="title">Cena za media</label>
                                <input type="text" class="form-control" id="fees" name="fees"
                                       value="{{ old('fees') }}"
                                       placeholder="300">
                            </div>
                        </div>

                        <div>
                            <label for="title">Dodaj zdjęcia</label>

                            <div class="form-row mb-3">
                                <div class="col-md-6">
                                    <div class="input-group mb-3">
                                        <div class="custom-file">
                                            <input type="file" class="custom-file-input" name="img_1" id="img_1">
                                            <label class="custom-file-label" for="img_1">zdjęcie 1</label>
                                        </div>
                                    </div>
                                </div>
                                <div class="col-md-6">
                                    <div class="custom-file">
                                        <input type="file" class="custom-file-input" name="img_2" id="img_2">
                                        <label class="custom-file-label" for="img_2">zdjęcie 2</label>
                                    </div>
                                </div>
                            </div>

                            <div class="form-row mb-3">
                                <div class="col-md-6">
                                    <div class="input-group mb-3">
                                        <div class="custom-file">
                                            <input type="file" class="custom-file-input" name="img_3" id="img_3">
                                            <label class="custom-file-label" for="img_3">zdjęcie 3</label>
                                        </div>
                                    </div>
                                </div>
                                <div class="col-md-6">
                                    <div class="custom-file">
                                        <input type="file" class="custom-file-input" name="img_4" id="img_4">
                                        <label class="custom-file-label" for="img_4">zdjęcie 4</label>
                                    </div>
                                </div>
                            </div>
                        </div>

                        <div class="form-row">
                            <div class="col-md-12 mb-3">
                                <label for="title">Opis</label>
                                <input id="description" type="hidden" name="description">
                                <trix-editor input="description">{!! old('description') !!}</trix-editor>
                            </div>
                        </div>

                        <div class="form-group text-center my-5">
                            <button class="btn btn-success btn-block" type="submit">dodaj mieszkanie</button>
                        </div>

                    </form>
                </div>
            </div>
        </div>
@endsection

