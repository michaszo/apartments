@extends('layouts.app')

@section('content')
    <div class="row text-center">
        <div class="col-12">
            <h4>Wybierz mieszkanie:</h4>
            <ul class="list-group text-center mt-3">
                @forelse($apartments as $apartment)

                    <li class="list-group-item bg-light">
                        <a href="{{route('forum.show', $apartment->id)}}">{{$apartment->title}}</a>
                    </li>

                @empty
                    <li class="list-group-item bg-light">
                        <h6>Dodaj mieszkanie, aby móc korzystać z forum</h6>
                    </li>
                @endforelse

            </ul>
        </div>
    </div>
@endsection

