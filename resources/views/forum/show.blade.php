@extends('layouts.app')

@section('content')
    <h4 class="mb-3 text-center">Forum mieszkania {{$apartment->title}}</h4>

    <div class="row">
        <div class="col-12">
            @include('forum.create')
        </div>
    </div>

    <div class="p-3 rounded shadow-sm">
        @foreach($posts as $post)
            <div class="text-muted pt-3">
                <p class="row text-gray-dark">
                <span
                    class="col-8 {{ ((new \Carbon\Carbon())->diffInMinutes($post->created_at) < 6*24 ? 'text-success' : 'text-primary' )  }}">
                    <strong>
                        {{$post->author->name}}
                    </strong>
                </span>
                    <span class="col-4 text-right">
                    <strong>
                        {{$post->created_at}}
                    </strong>
                </span>
                </p>
                <div class="border-bottom border-gray">
                    {!! $post->content !!}
                </div>
            </div>
        @endforeach
    </div>

@endsection

