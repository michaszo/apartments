<div class="row">
    <div class="col-12">
        <div class="card-body">
            <form action="{{route('forum.store')}}" method="post" enctype="multipart/form-data">
                @csrf
                <div class="form-row">
                    <div class="col-md-12 mb-3 ">
                        <input id="post" type="hidden" name="post">
                        <trix-editor input="post" placeholder="treść posta">{!! old('post') !!}</trix-editor>
                    </div>
                    <input type="text" hidden value="{{$apartment->id}}" name="apartment_id">
                </div>
                <div class="form-group text-center">
                    <button class="btn btn-success btn" type="submit">dodaj post</button>
                </div>
            </form>
        </div>
    </div>
</div>
