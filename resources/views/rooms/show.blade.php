@extends('layouts.app')

@section('content')

    <div class="row">
        <div class="col-12">
            <div class="card card-default">
                <div class="card-header">
                    <h6>Numer ogłoszenia: {{$room->id}}</h6>
                    <h6>Najemca: {{$rent->tenant->name}}</h6>
                    <h6>Numer telefonu: {{$rent->tenant->phone_number}}</h6>
                </div>
                <div class="card-body">
                    <ul class="list-group list-group-flush">

                        <li class="list-group-item d-flex justify-content-between">
                            <span>
                                <span>Umowa do:</span>
                                <span class="font-weight-bold">{{$rent->contract_to}}</span>
                            </span>
                            <span>
                                <span>Umowa od:</span>
                                <span class="font-weight-bold">{{$rent->created_at}}</span>
                            </span>
                        </li>

                        <li class="list-group-item">
                            <span>Tytuł:</span>
                            <span class="font-weight-bold">{{$room->title}}</span>
                        </li>

                        <li class="list-group-item">
                            <span>Mieszkanie:</span>
                            <span class="font-weight-bold">{{$room->apartment->title}}</span>
                        </li>

                        <li class="list-group-item d-flex justify-content-between">
                            <span>
                                <span>Ilość łóżek:</span>
                                <span class="font-weight-bold">{{$room->number_of_beds}}</span>
                            </span>
                            <span>
                                <span>Cena wynajmu (za całość):</span>
                                <span class="font-weight-bold">{{$rent->rental_price}}</span>
                            </span>
                            <span>
                                <span>Opłaty:</span>
                                <span class="font-weight-bold">{{$rent->fees}}</span>
                            </span>
                        </li>

                        <li class="list-group-item ">
                            <h6>Notatki dotyczące umowy:</h6>
                            <p>{{$rent->notes}}</p>
                        </li>

                        <li class="list-group-item">
                            <div class="mt-5">
                                <img src="{{ asset('storage/'. $room->img)}}" class="img-fluid" alt="zdjęcie pokoju">
                            </div>
                        </li>

                        <li class="list-group-item">
                            <span>Opis:</span>
                            <span class="font-weight-bold">{!! $room->description !!}</span>
                        </li>

                        <li class="list-group-item">
                            <span>Dodano:</span>
                            <span class="font-weight-bold">{{ $room->created_at }}</span>
                        </li>


                    </ul>
                </div>
            </div>
        </div>
    </div>
@endsection

