@extends('layouts.app')

@section('content')
    <div class="row">
        <div class="col-12">
            <div class="card card-default">
                <div class="card-header">
                    <h5>dodawanie nowego pokoju</h5>
                </div>
                <div class="card-body">
                    <form action="{{route('rooms.store')}}" method="post" enctype="multipart/form-data">
                        @csrf

                        @if($id !== null)
                            <div class="form-row">
                                <div class="col-md-12 mb-3">
                                    <label for="title">Mieszkanie</label>
                                    <select class="custom-select" id="apartment_id" name="apartment_id">
                                        <option value="{{$apartments->id}}">
                                            {{$apartments->title}}
                                        </option>
                                    </select>
                                </div>
                            </div>
                        @else
                            <div class="form-row">
                                <div class="col-md-12 mb-3">
                                    <label for="title">Mieszkanie</label>
                                    <select class="custom-select" id="apartment_id" name="apartment_id">
                                        @foreach($apartments as $apartment)
                                            <option value="{{$apartment->id}}">
                                                {{$apartment->title}}
                                            </option>
                                        @endforeach
                                    </select>
                                </div>
                            </div>
                        @endif

                        <div class="form-row">
                            <div class="col-md-12 mb-3">
                                <label for="title">Tytuł</label>
                                <input type="text" class="form-control" id="title" name="title"
                                       value="{{ old('title') }}"
                                       placeholder="Pokój dla dwóch osób, od strony wschodniej">
                            </div>
                        </div>


                        <div class="form-row">
                            <div class="col-md-3 mb-3">
                                <label for="title">Ilość łóżek</label>
                                <input type="text" class="form-control" id="number_of_beds" name="number_of_beds"
                                       value="{{ old('number_of_beds') }}"
                                       placeholder="2">
                            </div>
                            <div class="col-md-4 offset-1 mb-3">
                                <label for="title">Cena za wynajem</label>
                                <input type="text" class="form-control" id="rental_price" name="rental_price"
                                       value="{{ old('rental_price') }}"
                                       placeholder="800">
                            </div>
                            <div class="col-md-3 offset-1 mb-3">
                                <label for="title">Cena za media</label>
                                <input type="text" class="form-control" id="fees" name="fees"
                                       value="{{ old('fees') }}"
                                       placeholder="100">
                            </div>
                        </div>

                        <div>
                            <label for="title">Dodaj zdjęcie</label>

                            <div class="form-row mb-3">

                                <div class="input-group mb-3">
                                    <div class="custom-file">
                                        <input type="file" class="custom-file-input" name="img" id="img">
                                        <label class="custom-file-label" for="img">zdjęcie pokoju</label>
                                    </div>
                                </div>

                            </div>
                        </div>

                        <div class="form-row">
                            <div class="col-md-12 mb-3">
                                <label for="title">Opis</label>
                                <input id="description" type="hidden" name="description">
                                <trix-editor input="description">{!! old('description') !!}</trix-editor>
                            </div>
                        </div>

                        <div class="form-group text-center my-5">
                            <button class="btn btn-success btn-block" type="submit">dodaj pokój</button>
                        </div>

                    </form>
                </div>
            </div>
        </div>
@endsection

