@extends('layouts.app')

@section('content')
    <div class="row">
        <div class="col-12">
            @forelse($rooms as $room)
                <div
                    class="pl-2 no-gutters border flex-md-row mb-4 shadow-sm h-md-250 position-relative">
                    <div class="row">

                        <div class="p-4 d-flex flex-column position-static {{($room->img) ? 'col-7' : 'col-12'}}">
                            <strong class="d-inline-block text-primary"><h4>{{$room->city}} {{$room->street}}</h4>
                            </strong>
                            <div class="text-muted small">{{$room->created_at}}</div>
                            <h4 class="mt-3">{{$room->title}}</h4>
                            <p class="card-text mb-auto">{!!  $room->description !!}</p>
                            <a href="{{route('rooms.show', $room->id)}}">
                                <button type="button" class="btn btn-outline-primary btn-block   px-1">szczegóły</button>
                            </a>
                        </div>
                        @if($room->img)
                            <div class="col-5 d-none d-lg-block">
                                <div class="mt-5 p-3">
                                    <img src="{{ asset('storage/'. $room->img)}}" class="img-fluid">
                                </div>
                            </div>
                        @endif
                    </div>
                </div>
            @empty
            @endforelse
        </div>
    </div>
@endsection
