@extends('layouts.app')

@section('content')
    <div class="row">
        <div class="col-12">

            <div class="row text-center bg-light p-2 pt-4 border flex-md-row mb-4 shadow-sm h-md-250">
                <div class="col-lg-12">
                    <h5 class="mt-1 text-primary mb-4">Informacje o wynajmie:</h5>
                    <div class="row">
                        <div class="col-lg-3">
                            <h6 class="mt-1">Czynsz:</h6>
                            <h5 class="text-info">{{$rent->rental_price}}</h5>
                        </div>
                        <div class="col-lg-3">
                            <h6 class="mt-1">Media:</h6>
                            <h5 class="text-info">{{$rent->fees}}</h5>
                        </div>
                        <div class="col-lg-3">
                            <h6 class="mt-1">Wynajem od:</h6>
                            <h5 class="text-info">{{ \Carbon\Carbon::parse($rent->created_at)->format('d/m/Y')}}</h5>
                        </div>
                        <div class="col-lg-3">
                            <h6 class="mt-1">Wynajem do:</h6>
                            <h5 class="text-info">{{ \Carbon\Carbon::parse($rent->contract_to)->format('d/m/Y')}}</h5>
                        </div>

                    </div>
                </div>
            </div>


            <div class="row text-center p-2 pt-4 border flex-md-row mb-4 shadow-sm h-md-250">
                <div class="col-lg-12">
                    <h5 class="mt-1 text-primary mb-4">Właściciel mieszkania:</h5>
                    <div class="row">
                        <div class="col-lg-4">
                            <h6 class="mt-1">Właściciel:</h6>
                            <h5 class="text-info">{{$room->owner->name}}</h5>
                        </div>
                        <div class="col-lg-4">
                            <h6 class="mt-1">email:</h6>
                            <h5 class="text-info">{{$room->owner->email}}</h5>
                        </div>
                        <div class="col-lg-4">
                            <h6 class="mt-1">numer telefonu:</h6>
                            <h5 class="text-info">{{$room->owner->phone_number}}</h5>
                        </div>
                    </div>
                </div>
            </div>

            <div class="row text-center bg-light p-2 pt-4 border flex-md-row mb-4 shadow-sm h-md-250">
                <div class="col-lg-12">
                    <h5 class="mt-1 text-primary mb-4">Informacje o mieszkaniu:</h5>
                    <div class="row">
                        <div class="col-lg-9">
                            <h6 class="mt-1">Tytuł:</h6>
                            <h5 class="text-info">{{$room->title}}</h5>
                        </div>
                        <div class="col-lg-3">
                            <h6 class="mt-1">Adres:</h6>
                            <h5 class="text-info">{{$room->apartment->city}}</h5>
                            <h5 class="text-info">{{$room->apartment->street}}</h5>
                            <h5 class="text-info">{{$room->apartment->apartment_number}}</h5>
                        </div>

                    </div>
                </div>
            </div>
        </div>
    </div>
@endsection

