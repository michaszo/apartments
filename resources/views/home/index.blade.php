@extends('layouts.app')

@section('content')
    <div class="row text-center">
        <div class="col-12">
            <h1 class="text-center">Twoje ID to: {{\Illuminate\Support\Facades\Auth::id()}}</h1>
        </div>
    </div>
@endsection

