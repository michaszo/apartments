@extends('layouts.app')

@section('content')
    <div class="row text-center">
        <div class="col-12 table-responsive table-hover">
            <table class="table table-sm">
                <thead class="thead-dark">
                <tr>
                    <th scope="col">mieszkanie</th>
                    <th scope="col">lokator</th>
                    <th scope="col">nr do lokatora</th>
                    <th scope="col">umowa od-do</th>
                    <th scope="col">czynsz</th>
                    <th scope="col">opłaty</th>
                </tr>
                </thead>
                <tbody>
                @forelse($rents as $rent)
                    <tr>
                        <td>{{$rent->apartment->city}} {{$rent->apartment->street}} {{$rent->apartment->number}}</td>
                        <td>{{$rent->tenant->name}}</td>
                        <td>{{$rent->tenant->phone_number}}</td>
                        <td>{{ \Carbon\Carbon::parse($rent->created_at)->format('d/m/Y')}} - {{ \Carbon\Carbon::parse($rent->contract_to)->format('d/m/Y')}}</td>
                        <td>{{$rent->rental_price}}PLN</td>
                        <td>{{$rent->fees}}PLN</td>
                    </tr>
                @empty
{{--                    <h2>Brak rozliczeń</h2>--}}
                @endforelse
                </tbody>
            </table>
        </div>
    </div>
@endsection

