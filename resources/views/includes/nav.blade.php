<div class="col-md-3">

    <ul class="list-group text-center">
        <li class="list-group-item bg-light">
            <a href="{{route('home.index')}}">przegląd</a>
        </li>
    </ul>

    @if(!auth()->user()->isOwner())
        <ul class="list-group text-center mt-3">
            <li class="list-group-item bg-light">
                <a href="{{route('forum.tenant', \Illuminate\Support\Facades\Auth::id())}}">forum</a>
            </li>
        </ul>
    @endif


    @if(auth()->user()->isOwner())
        <ul class="list-group text-center mt-3">
            <li class="list-group-item bg-light">
                <a href="{{route('forum.index')}}">fora</a>
            </li>


            <li class="list-group-item bg-light">
                <a href="{{route('note.create')}}">notatki</a>
            </li>

            <li class="list-group-item bg-light">
                <a href="{{route('finance.index')}}">finanse</a>
            </li>
        </ul>

        <ul class="list-group text-center mt-3">


            <li class="list-group-item bg-light">
                <a href="{{route('apartments.index')}}">twoje mieszkania</a>
            </li>

            <li class="list-group-item bg-light">
                <a href="{{route('rooms.index')}}">twoje pokoje</a>
            </li>

        </ul>

        <ul class="list-group text-center mt-3">

            <li class="list-group-item bg-light">
                <a href="{{route('apartments.create')}}">dodaj nowe mieszkanie</a>
            </li>

            <li class="list-group-item bg-light">
                <a href="{{route('rent.index')}}">dodaj lokatora do mieszkania</a>
            </li>

        </ul>
    @endif


</div>
