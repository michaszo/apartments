@extends('layouts.app')

@section('content')
    <div class="row">
        <div class="col-12">

            <form method="GET" action="{{route('search')}}" class="row">
                <div class="col-8 offset-1">
                    <input class="form-control mr-sm-2" name="q" type="search"
                           placeholder="wpisz miasto, ulicę lub inne słowo klucz" aria-label="Search">
                </div>
                <div class="col-2">
                    <button class="btn btn-outline-success " type="submit">wyszukaj</button>
                </div>
            </form>

            <div class="card-body">
                @forelse($apartments as $apartment)
                    <div>
                        <div class="row">
                            <div class="col-12">

                                <div
                                    class="row no-gutters border rounded overflow-hidden flex-md-row mb-4 shadow-sm h-md-250 position-relative">
                                    <div class="col-7 p-4 d-flex flex-column position-static">

                                        <strong class="d-inline-block text-primary"><h3>{{$apartment->city}}</h3></strong>
                                        <div class="text-muted small">{{$apartment->created_at}}</div>
                                        <h3 class="mt-3">{{$apartment->title}}</h3>
                                        <p class="card-text mb-auto">{!!  $apartment->description !!}</p>
                                        <strong class="d-inline-block text-primary mt-2"><h5>Opłaty: {!!  $apartment->rental_price + $apartment->fees !!}PLN</h5></strong>
                                        <a href="{{route('app.show', $apartment->id)}}" class="btn btn-outline-success">zobacz
                                            szczegóły</a>
                                    </div>
                                    <div class="col-5 p-2 d-none d-lg-block">
                                        <div class="mt-5">
                                            <img src="{{ asset('storage/'. $apartment->img_1)}}" class="img-fluid">
                                        </div>
                                    </div>

                                </div>

                            </div>
                        </div>
                    </div>
                @empty
                    <div class="text-center text-secondary mt-5">
                        <h3>Nic nie znaleziono.</h3>
                        <h4>Spróbuj zmienić parametry wyszukiwania</h4>
                    </div>
                @endforelse
            </div>
        </div>
@endsection

