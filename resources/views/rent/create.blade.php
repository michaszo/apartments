@extends('layouts.app')

@section('content')
    <div class="row">
        <div class="col-12">
            <div class="card card-default">
                <div class="card-header">
                    <h5>przypisywanie najemcy do pokoju</h5>
                </div>
                <div class="card-body">
                    <form action="{{route('rent.store')}}" method="post" enctype="multipart/form-data">
                        @csrf

                        <div class="form-row">
                            <div class="col-md-12 mb-3">
                                <label for="title">Pokoje</label>
                                <select class="custom-select" id="room_id" name="room_id">
                                    @foreach($rooms as $room)
                                        <option value="{{$room->id}}">
                                            {{$room->title}}
                                        </option>
                                    @endforeach
                                </select>
                            </div>
                        </div>

                        <div class="form-row">
                            <div class="col-md-3 mb-3">
                                <label for="title">ID najemcy</label>
                                <input type="text" class="form-control" id="tenant_id" name="tenant_id"
                                       value="{{ old('tenant_id') }}"
                                       placeholder="13">
                            </div>
                            <div class="col-md-9 mb-3">
                                <label for="title">imię i nazwisko najemcy</label>
                                <input type="text" class="form-control" id="tenant_name" name="tenant_name"
                                       value="{{ old('tenant_name') }}"
                                       placeholder="Jarek Kowalski">
                            </div>
                        </div>

                        <div class="form-row">
                            <div class="col-md-4 mb-3">
                                <label for="title">Cena za wynajem</label>
                                <input type="text" class="form-control" id="rental_price" name="rental_price"
                                       value="{{ old('rental_price') }}"
                                       placeholder="800">
                            </div>
                            <div class="col-md-4 mb-3">
                                <label for="title">Cena za media</label>
                                <input type="text" class="form-control" id="fees" name="fees"
                                       value="{{ old('fees') }}"
                                       placeholder="100">
                            </div>
                            <div class="col-md-4 mb-3">
                                <label for="title">Umowa do</label>
                                <input type="date" class="form-control" id="contract_to" name="contract_to"
                                       value="{{ old('contract_to') }}"
                                       placeholder="21.01.2020">
                            </div>
                        </div>

                        <div class="form-row">
                            <div class="col-md-12 mb-3">
                                <label for="title">Notatki</label>
                                <input id="notes" type="hidden" name="notes">
                                <trix-editor input="notes">{!! old('notes') !!}</trix-editor>
                            </div>
                        </div>

                        <div class="form-group text-center my-5">
                            <button class="btn btn-success btn-block" type="submit">dodaj najemcę</button>
                        </div>

                    </form>
                </div>
            </div>
        </div>
@endsection

