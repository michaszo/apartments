@extends('layouts.app')

@section('content')
    <div class="row text-center">
        <div class="col-12">
            <h4>Wybierz pokój:</h4>
            <ul class="list-group text-center mt-3">
                @forelse($apartments as $apartment)

                    <li class="list-group-item bg-light font-weight-bold">
                        <a href="{{route('forum.show', $apartment->id)}}">{{$apartment->title}}</a>
                    </li>

                    @foreach($apartment->rooms as $room)

                        <li class="list-group-item ">
                            <a href="{{route('rent.create', $room->id)}}">{{$room->id}}. {{$room->title}}</a>
                        </li>

                    @endforeach

                @empty
                    <li class="list-group-item bg-light">
                        <h6>Dodaj mieszkanie, aby móc korzystać z forum</h6>
                    </li>
                @endforelse

            </ul>
        </div>
    </div>
@endsection

