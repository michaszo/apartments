@extends('layouts.app')

@section('content')
    <div class="row">
        <div class="col-12">
            @include('note.notes')
        </div>
    </div>

    <div class="row mt-5">
        <div class="col-12">
            <form action="{{route('note.store')}}" method="post" enctype="multipart/form-data">
                @csrf
                <div class="form-row">
                    <div class="col-md-12 mb-3 ">
                        <label for="title">Notatka</label>
                        <input id="notes" type="hidden" name="notes">
                        <trix-editor input="notes">{!! old('notes') !!}</trix-editor>
                    </div>
                </div>

                <div class="form-row">
                    <div class="col-md-4 mb-3">
                        <label for="title">Mieszkanie</label>
                        <select class="custom-select" id="apartment_id" name="apartment_id">
                            <option value="">
                            </option>
                            @foreach($apartments as $apartment)
                                <option value="{{$apartment->id}}">
                                    {{$apartment->title}}
                                </option>
                            @endforeach
                        </select>
                    </div>
                    <div class="col-md-4 mb-3">
                        <label for="title">Pokój</label>
                        <select class="custom-select" id="room_id" name="room_id">
                            <option value="">
                            </option>
                            @foreach($rooms as $room)
                                <option value="{{$room->id}}">
                                    {{$room->title}}
                                </option>
                            @endforeach
                        </select>
                    </div>
                    <div class="col-md-4 mb-3">
                        <label for="title">Umowa (data_id_miesz_id_pok)</label>
                        <select class="custom-select" id="tenant_id" name="tenant_id">
                            <option value="">
                            </option>
                            @foreach($rents as $rent)
                                <option value="{{$rent->tenant_id}}">
                                    {{substr($rent->created_at, 0, 10)}}_{{$rent->apartment_id}}
                                    _{{$rent->room_id}}
                                </option>
                            @endforeach

                        </select>
                    </div>
                </div>

                <div class="form-group text-center">
                    <button class="btn btn-success" type="submit">dodaj notatkę</button>
                </div>

            </form>
        </div>
    </div>
@endsection

