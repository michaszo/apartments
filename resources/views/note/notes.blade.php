@foreach($notes as $note)
    <div class="text-muted pt-3">
        <p class="row text-gray-dark">
                <span
                    class="col-8 {{ ((new \Carbon\Carbon())->diffInMinutes($note->created_at) < 6*24 ? 'text-success' : 'text-primary' )  }}">
                    <strong>
                        @if((new \Carbon\Carbon())->diffInMinutes($note->created_at) < 60*24)
                            <span class="badge badge-success">nowa!</span>
                        @endif
                        notatka #{{$note->id}}
                    </strong>
                    @if($note->tenant_id)
                        ID najemcy: {{$note->tenant_id}}
                    @endif
                    @if($note->tenant_id)
                        ID pokoju: {{$note->room_id}}
                    @endif
                    @if($note->tenant_id)
                        ID mieszkania: {{$note->apartment_id}}
                    @endif
                </span>
            <span class="col-4 text-right">
                    <strong>
                        {{$note->created_at}}
                    </strong>
                </span>
        </p>
        <div class="border-bottom border-gray">
            {!! $note->notes !!}
        </div>
    </div>
@endforeach
<nav class="mt-4">
    <ul class="pagination justify-content-center">
        {{$notes}}
    </ul>
</nav>

