<?php

namespace App;

use Illuminate\Database\Eloquent\Model;
use Illuminate\Support\Facades\Storage;

class Apartment extends Model
{
    protected $fillable = ['title', 'owner_id', 'city', 'street', 'apartment_number',
        'number_of_beds', 'rental_price', 'fees', 'description',
        'img_1', 'img_2', 'img_3', 'img_4'];

    public function deleteImage($img)
    {
        Storage::delete($img);
    }

    public function owner()
    {
        return $this->belongsTo(User::class);
    }

    public function rooms()
    {
        return $this->hasMany(Room::class);
    }

    public function finances()
    {
        return $this->hasMany(Finance::class);
    }

    public function rent()
    {
        return $this->hasOne(Apartment::class, 'apartment_id');
    }
}
