<?php

namespace App;

use Illuminate\Database\Eloquent\Model;

class Note extends Model
{
    protected $fillable = ['owner_id', 'tenant_id', 'apartment_id', 'room_id', 'notes'];
}
