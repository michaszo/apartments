<?php

namespace App;

use Illuminate\Database\Eloquent\Model;

class Forum extends Model
{
    protected $fillable = ['author_id', 'apartment_id', 'content'];

    public function author()
    {
        return $this->belongsTo(User::class);
    }
}
