<?php

namespace App\Http\Requests;

use Illuminate\Foundation\Http\FormRequest;

class ApartamentCreateRequest extends FormRequest
{
    /**
     * Determine if the user is authorized to make this request.
     *
     * @return bool
     */
    public function authorize()
    {
        return true;
    }

    /**
     * Get the validation rules that apply to the request.
     *
     * @return array
     */
    public function rules()
    {
        return [
            'title' => 'required|min:3|max:40',
            'city' => 'required',
            'street' => 'required',
            'apartment_number' => 'required',
            'number_of_beds' => 'required',
            'rental_price' => 'required',
            'fees' => 'required',
            'description' => 'required|min:10|max:255',
            'img_1' => 'required|image', //TODO:Test image
            'img_2' => 'nullable',
            'img_3' => 'nullable',
            'img_4' => 'nullable'
        ];
    }
}
