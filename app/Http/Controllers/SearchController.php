<?php

namespace App\Http\Controllers;

use App\Apartment;
use Illuminate\Http\Request;

class SearchController extends Controller
{
    public function apartments(Request $request)
    {

        $cities = Apartment::select('id')
            ->where('city', 'like', '%' . $request->q . '%')
            ->get();

        $streets = Apartment::select('id')
            ->where('street', 'like', '%' . $request->q . '%')
            ->get();

        $titles = Apartment::select('id')
            ->where('title', 'like', '%' . $request->q . '%')
            ->get();

        $ids = [];

        foreach ($cities as $city) {
            $ids[] = $city->id;
        }

        foreach ($streets as $street) {
            $ids[] = $street->id;
        }

        foreach ($titles as $title) {
            $ids[] = $title->id;
        }

        $ids = array_unique($ids);

        $apartments = [];

        foreach ($ids as $id) {
            $apartments[] = Apartment::findOrFail($id);
        }

        return view('app.index', compact('apartments'));
    }
}
