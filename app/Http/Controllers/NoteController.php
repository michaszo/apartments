<?php

namespace App\Http\Controllers;

use App\Apartment;
use App\Http\Requests\NoteCreateRequest;
use App\Note;
use App\Rent;
use App\Room;
use Illuminate\Http\Request;
use Illuminate\Support\Facades\Auth;

class NoteController extends Controller
{
    /**
     * Display a listing of the resource.
     *
     * @return \Illuminate\Http\Response
     */
    public function index()
    {

    }

    /**
     * Show the form for creating a new resource.
     *
     * @return \Illuminate\Http\Response
     */
    public function create()
    {
        $apartments = Apartment::all()->where('owner_id', Auth::id());
        $rooms = Room::all()->where('owner_id', Auth::id());
        $rents = Rent::all()->where('owner_id', Auth::id())->sortBy('apartment_id');
        $notes = Note::latest()->where('owner_id', Auth::id())->paginate(6);
        return view('note.create', compact('apartments', 'rooms', 'rents', 'notes'));
    }

    /**
     * Store a newly created resource in storage.
     *
     * @param \Illuminate\Http\Request $request
     * @return \Illuminate\Http\Response
     */
    public function store(NoteCreateRequest $request)
    {
        $owner_id =  Auth::id();

        Note::create([
            'owner_id' => $owner_id,
            'tenant_id' => $request->tenant_id,
            'apartment_id' => $request->apartment_id,
            'room_id' => $request->room_id,
            'notes' => $request->notes
        ]);

        return redirect()->route('note.create')
            ->with('success', 'Notatka dodana pomyślnie.');
    }

    /**
     * Display the specified resource.
     *
     * @param \App\Note $note
     * @return \Illuminate\Http\Response
     */
    public function show(Note $note)
    {
        //
    }

    /**
     * Show the form for editing the specified resource.
     *
     * @param \App\Note $note
     * @return \Illuminate\Http\Response
     */
    public function edit(Note $note)
    {
        //
    }

    /**
     * Update the specified resource in storage.
     *
     * @param \Illuminate\Http\Request $request
     * @param \App\Note $note
     * @return \Illuminate\Http\Response
     */
    public function update(Request $request, Note $note)
    {
        //
    }

    /**
     * Remove the specified resource from storage.
     *
     * @param \App\Note $note
     * @return \Illuminate\Http\Response
     */
    public function destroy(Note $note)
    {
        //
    }
}
