<?php

namespace App\Http\Controllers;

use App\Apartment;
use App\Finance;
use App\Rent;
use DateTime;
use Illuminate\Http\Request;
use Illuminate\Support\Facades\Auth;

class FinanceController extends Controller
{
    /**
     * Display a listing of the resource.
     *
     * @return \Illuminate\Http\Response
     */
    public function index()
    {
        $finances = Finance::all()->where('owner_id', Auth::id());
        $rentals = Rent::all()->where('owner_id', Auth::id());
        $today = new DateTime(date('Y-m-d\TH:i'));

        $rental_profit = 0;
        $fees = 0;

        foreach ($rentals as $rental) {
            $interval = $today->diff($rental->created_at);
            $months = $interval->format('%m');
            foreach (range(0, $months) as $i) {
                $rental_profit += $rental->rental_price;
                $fees -= $rental->fees;
            }

        }

        $income = 0;
        $outgo = 0;

        foreach ($finances as $finance) {
            if ($finance->type === 'income') {
                $income += $finance->amount;
            } else {
                $outgo += $finance->amount;
            }
        }

        $profit = ($rental_profit + $income) - ($outgo - $fees);

        return view('finance.index', compact('finances', 'income', 'outgo', 'profit', 'rental_profit', 'fees'));
    }

    /**
     * Show the form for creating a new resource.
     *
     * @return \Illuminate\Http\Response
     */
    public function create()
    {
        //
    }

    /**
     * Store a newly created resource in storage.
     *
     * @param \Illuminate\Http\Request $request
     * @return \Illuminate\Http\Response
     */
    public function store(Request $request)
    {
        //
    }

    /**
     * Display the specified resource.
     *
     * @param \App\Finance $finance
     * @return \Illuminate\Http\Response
     */
    public function show(Finance $finance)
    {
        //
    }

    /**
     * Show the form for editing the specified resource.
     *
     * @param \App\Finance $finance
     * @return \Illuminate\Http\Response
     */
    public function edit(Finance $finance)
    {
        //
    }

    /**
     * Update the specified resource in storage.
     *
     * @param \Illuminate\Http\Request $request
     * @param \App\Finance $finance
     * @return \Illuminate\Http\Response
     */
    public function update(Request $request, Finance $finance)
    {
        //
    }

    /**
     * Remove the specified resource from storage.
     *
     * @param \App\Finance $finance
     * @return \Illuminate\Http\Response
     */
    public function destroy(Finance $finance)
    {
        //
    }
}
