<?php

namespace App\Http\Controllers;

use App\Apartment;
use App\Forum;
use App\Rent;
use Illuminate\Http\Request;
use Illuminate\Support\Facades\Auth;

class ForumController extends Controller
{
    /**
     * Display a listing of the resource.
     *
     * @return \Illuminate\Http\Response
     */
    public function index()
    {
        $apartments = Apartment::all()->where('owner_id', Auth::id());
        return view('forum.index', compact('apartments'));
    }

    /**
     * Show the form for creating a new resource.
     *
     * @return \Illuminate\Http\Response
     */
    public function create()
    {
    }

    /**
     * Store a newly created resource in storage.
     *
     * @param \Illuminate\Http\Request $request
     * @return \Illuminate\Http\Response
     */
    public function store(Request $request)
    {
        Forum::create([
            'apartment_id' => $request->apartment_id,
            'author_id' => Auth::id(),
            'content' => $request->post
        ]);

        return redirect()->route('forum.show', $request->apartment_id)
            ->with('success', 'Post dodany pomyślnie.');
    }

    /**
     * Display the specified resource.
     *
     * @param \App\Forum $forum
     * @return \Illuminate\Http\Response
     */
    public function show($id)
    {
        $apartment = Apartment::find($id);
        $posts = Forum::all()->where('apartment_id', $id)->sortByDesc('created_at');
        return view('forum.show', compact('posts', 'apartment'));

    }

    /**
     * Show the form for editing the specified resource.
     *
     * @param \App\Forum $forum
     * @return \Illuminate\Http\Response
     */
    public function edit(Forum $forum)
    {
        //
    }

    /**
     * Update the specified resource in storage.
     *
     * @param \Illuminate\Http\Request $request
     * @param \App\Forum $forum
     * @return \Illuminate\Http\Response
     */
    public function update(Request $request, Forum $forum)
    {
        //
    }

    /**
     * Remove the specified resource from storage.
     *
     * @param \App\Forum $forum
     * @return \Illuminate\Http\Response
     */
    public function destroy(Forum $forum)
    {
        //
    }

    public function tenant($id)
    {
        $rent = Rent::where('tenant_id', $id)->firstOrFail();
        $apartment_id = $rent->apartment_id;
        $apartment = Apartment::where('id', $apartment_id)->firstOrFail();

        $posts = Forum::all()->where('apartment_id', $apartment->id)->sortByDesc('created_at');

        return view('forum.show', compact('posts', 'apartment'));
    }

}
