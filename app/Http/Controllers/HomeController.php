<?php

namespace App\Http\Controllers;

use App\Rent;
use App\Room;
use Illuminate\Http\Request;
use Illuminate\Support\Facades\Auth;

class HomeController extends Controller
{
    /**
     * Create a new controller instance.
     *
     * @return void
     */
    public function __construct()
    {
        $this->middleware('auth');
    }

    /**
     * Show the application dashboard.
     *
     * @return \Illuminate\Contracts\Support\Renderable
     */
    public function index()
    {
        if (!auth()->user()->isOwner()) {
            $rent = Rent::where('tenant_id', Auth::id())->first();
            if ($rent === null) {
                return view('home.index');
            } else {
                $room = Room::where('owner_id', $rent->owner_id)->firstOrFail();
                return view('home.tenant', compact('rent', 'room'));
            }
        } else {
            $rents = Rent::All()->where('owner_id', Auth::id());
            return view('home.owner', compact('rents'));
        }
    }
}
