<?php

namespace App\Http\Controllers;

use App\Apartment;
use App\Http\Requests\ApartamentCreateRequest;
use App\Note;
use App\Room;
use App\User;
use Illuminate\Http\Request;
use Illuminate\Support\Facades\Auth;

class ApartmentsController extends Controller
{
    /**
     * Display a listing of the resource.
     *
     * @return \Illuminate\Http\Response
     */
    public function index()
    {
        $apartments = Apartment::all()->where('owner_id', Auth::id());
        $rooms = Room::all();
        return view('apartments.index', compact('apartments', 'rooms'));
    }

    /**
     * Show the form for creating a new resource.
     *
     * @return \Illuminate\Http\Response
     */
    public function create()
    {
        return view('apartments.create');
    }

    /**
     * Store a newly created resource in storage.
     *
     * @param \Illuminate\Http\Request $request
     * @return \Illuminate\Http\Response
     */
    public function store(ApartamentCreateRequest $request)
    {
        $img_1 = (isset($request->img_1) ? $request->img_1->store('apartments') : null);
        $img_2 = (isset($request->img_2) ? $request->img_2->store('apartments') : null);
        $img_3 = (isset($request->img_3) ? $request->img_3->store('apartments') : null);
        $img_4 = (isset($request->img_4) ? $request->img_4->store('apartments') : null);

        $owner_id = Auth::id();

        Apartment::create([
            'title' => $request->title,
            'owner_id' => $owner_id,
            'city' => $request->city,
            'street' => $request->street,
            'apartment_number' => $request->apartment_number,
            'number_of_beds' => $request->number_of_beds,
            'rental_price' => $request->rental_price,
            'fees' => $request->fees,
            'description' => $request->description,
            'img_1' => $img_1,
            'img_2' => $img_2,
            'img_3' => $img_3,
            'img_4' => $img_4,
        ]);


        return redirect()->route('apartments.index')
            ->with('success', 'Mieszkanie dodane pomyślnie.');
    }

    /**
     * Display the specified resource.
     *
     * @param int $id
     * @return \Illuminate\Http\Response
     */
    public function show(Apartment $apartment)
    {
        $notes = Note::latest()->where('apartment_id', 2)->paginate(5);
        return view('apartments.show', compact('apartment', 'notes'));
    }

    /**
     * Show the form for editing the specified resource.
     *
     * @param int $id
     * @return \Illuminate\Http\Response
     */
    public function edit(Apartment $apartment)
    {
        return view('apartments.edit', compact('apartment'));
    }

    /**
     * Update the specified resource in storage.
     *
     * @param \Illuminate\Http\Request $request
     * @param int $id
     * @return \Illuminate\Http\Response
     */
    public function update(Request $request, Apartment $apartment)
    {
        $imgs = ['img_1', 'img_2', 'img_3', 'img_4'];

        foreach ($imgs as $key => $img) {
            if ($request->hasFile($img)) {
                $apartment->deleteImage($apartment->$img);
                $imgs[$key] = $request->$img->store('apartments');
            } else {
                $imgs[$key] = $apartment->$img;
            }
        }

        $apartment->update([
            'title' => $request->title,
            'city' => $request->city,
            'street' => $request->street,
            'apartment_number' => $request->apartment_number,
            'number_of_beds' => $request->number_of_beds,
            'rental_price' => $request->rental_price,
            'fees' => $request->fees,
            'description' => $request->description,
            'img_1' => $imgs[0],
            'img_2' => $imgs[1],
            'img_3' => $imgs[2],
            'img_4' => $imgs[3],
        ]);

        return redirect()
            ->back()
            ->with('success', 'Mieszkanie poprawnie uaktualnione');
    }

    /**
     * Remove the specified resource from storage.
     *
     * @param int $id
     * @return \Illuminate\Http\Response
     */
    public function destroy($id)
    {
        //
    }
}
