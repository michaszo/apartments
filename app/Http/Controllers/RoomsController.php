<?php

namespace App\Http\Controllers;

use App\Apartment;
use App\Rent;
use App\Room;
use Illuminate\Http\Request;
use Illuminate\Support\Facades\Auth;

class RoomsController extends Controller
{
    /**
     * Display a listing of the resource.
     *
     * @return \Illuminate\Http\Response
     */
    public function index()
    {
        $rooms = Room::all()->where('owner_id', Auth::id());
        return view('rooms.index', compact('rooms'));
    }

    /**
     * Show the form for creating a new resource.
     *
     * @return \Illuminate\Http\Response
     */
    public function create($id = null)
    {
        if ($id !== null) {
            $apartments = Apartment::findOrFail($id);
        } else {
            $apartments = Apartment::all()->where('owner_id', Auth::id());
        }

        return view('rooms.create', compact('apartments', 'id'));
    }

    /**
     * Store a newly created resource in storage.
     *
     * @param \Illuminate\Http\Request $request
     * @return \Illuminate\Http\Response
     */
    public function store(Request $request)
    {
        $img = (isset($request->img) ? $request->img->store('rooms') : null);

        $owner_id = Auth::id();

        Room::create([
            'title' => $request->title,
            'owner_id' => $owner_id,
            'apartment_id' => $request->apartment_id,
            'number_of_beds' => $request->number_of_beds,
            'rental_price' => $request->rental_price,
            'fees' => $request->fees,
            'description' => $request->description,
            'img' => $img,
        ]);

        return redirect()->route('apartments.index')
            ->with('success', 'Pokój dodany pomyślnie.');
    }

    /**
     * Display the specified resource.
     *
     * @param int $id
     * @return \Illuminate\Http\Response
     */
    public function show(Room $room)
    {
        $rent = Rent::where('room_id', $room->id)->firstOrFail();
        return view('rooms.show', compact('room', 'rent'));
    }

    /**
     * Show the form for editing the specified resource.
     *
     * @param int $id
     * @return \Illuminate\Http\Response
     */
    public function edit(Room $room)
    {
        $apartments = Apartment::all()->where('owner_id', Auth::id());
        return view('rooms.edit', compact('apartments', 'room'));
    }

    /**
     * Update the specified resource in storage.
     *
     * @param \Illuminate\Http\Request $request
     * @param int $id
     * @return \Illuminate\Http\Response
     */
    public function update(Request $request, $id)
    {
        dd($request->all());
    }

    /**
     * Remove the specified resource from storage.
     *
     * @param int $id
     * @return \Illuminate\Http\Response
     */
    public function destroy($id)
    {
        //
    }
}
