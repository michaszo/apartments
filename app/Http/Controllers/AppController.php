<?php

namespace App\Http\Controllers;

use App\Apartment;
use App\app;
use Illuminate\Http\Request;

class AppController extends Controller
{
    /**
     * Display a listing of the resource.
     *
     * @return \Illuminate\Http\Response
     */
    public function index()
    {
        $apartments = Apartment::latest()->paginate(5);
        return view('app.index', compact('apartments'));
    }

    /**
     * Show the form for creating a new resource.
     *
     * @return \Illuminate\Http\Response
     */
    public function create()
    {
        //
    }

    /**
     * Store a newly created resource in storage.
     *
     * @param  \Illuminate\Http\Request  $request
     * @return \Illuminate\Http\Response
     */
    public function store(Request $request)
    {
        //
    }

    /**
     * Display the specified resource.
     *
     * @param  \App\app  $app
     * @return \Illuminate\Http\Response
     */
    public function show($id)
    {
        $apartment = Apartment::findOrFail($id);
        return view('app.show', compact('apartment'));
    }

    /**
     * Show the form for editing the specified resource.
     *
     * @param  \App\app  $app
     * @return \Illuminate\Http\Response
     */
    public function edit(app $app)
    {
        //
    }

    /**
     * Update the specified resource in storage.
     *
     * @param  \Illuminate\Http\Request  $request
     * @param  \App\app  $app
     * @return \Illuminate\Http\Response
     */
    public function update(Request $request, app $app)
    {
        //
    }

    /**
     * Remove the specified resource from storage.
     *
     * @param  \App\app  $app
     * @return \Illuminate\Http\Response
     */
    public function destroy(app $app)
    {
        //
    }
}
