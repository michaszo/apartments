<?php

namespace App\Http\Controllers;

use App\Apartment;
use App\Rent;
use App\Room;
use App\User;
use Illuminate\Http\Request;
use Illuminate\Support\Facades\Auth;

class RentController extends Controller
{
    public function index()
    {
        $apartments = Apartment::all()->where('owner_id', Auth::id());
        return view('rent.index', compact('apartments'));
    }

    public function create($id)
    {
        $rooms = Room::all()->where('apartment_id', $id);
        return view('rent.create', compact('rooms'));
    }

    public function store(Request $request)
    {
        $tenant = User::find($request->tenant_id);
        $owner_id =  Auth::id();

        if (!isset($tenant)) {
            return redirect()->back()
                ->with('error', 'Dane najemcy się nie zgadzają.');
        }

        //Sprawdź zgodność podanych danych z danymi z db
        if ($request->tenant_name !== $tenant->name) {
            return redirect()->back()
                ->with('error', 'Dane najemcy się nie zgadzają.');
        }

        $apartment_id = Room::all()->where('id', $request->room_id)->first()->apartment_id;

        Rent::create([
            'owner_id' => $owner_id,
            'tenant_id' => $request->tenant_id,
            'apartment_id' => $apartment_id,
            'room_id' => $request->room_id,
            'rental_price' => $request->rental_price,
            'fees' => $request->fees,
            'contract_to' =>$request->contract_to,
            'notes' => $request->notes
        ]);


        return redirect()->route('apartments.index')
            ->with('success', 'Mieszkanie dodane pomyślnie.');
    }
}
