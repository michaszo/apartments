<?php

use Illuminate\Database\Seeder;
use Faker\Factory as Faker;

class FinanceSeeder extends Seeder
{
    /**
     * Run the database seeds.
     *
     * @return void
     */
    public function run()
    {
        $owners = \App\User::select('id')
            ->where('role', 'owner')
            ->orWhere('role', 'admin')->get();


        $types = ['income', 'outgo'];
        $faker = Faker::create();

        foreach ($owners as $owner) {
            $apartments = \App\Apartment::select('id')->where('owner_id', $owner->id)->get();

            foreach ($apartments as $apartment) {
                foreach (range(1, 3) as $index) {
                    DB::table('finances')->insert([
                        'title' => $faker->lexify('Zapłata za ???? ???????'),
                        'owner_id' => $owner->id,
                        'apartment_id' => $apartment->id,
                        'amount' => rand(10,700),
                        'type' => $types[rand(0, 1)],
                        'created_at' => $faker->dateTimeThisMonth($max = 'now', $timezone = null)
                    ]);
                }
            }
        }

    }
}
