<?php

use App\Apartment;
use App\User;
use Illuminate\Database\Seeder;
use Faker\Factory as Faker;

class ApartmentSeeder extends Seeder
{
    /**
     * Run the database seeds.
     *
     * @return void
     */
    public function run()
    {
        $cities = ['Kraków', 'Częstochowa', 'Poznań', 'Warszawa', 'Wrocław'];

        $imgs = [
            'apartments/1SOMTTzqRamBsHNuYBOt6tdOHDLNV1oRzuqqdzWZ.jpeg',
            'apartments/2zouAVNw7HTamNtd4IeWzWdE56BtW6ILJlhLla61.jpeg',
            'apartments/Aey4PaXhLoWlR1NAvxvyXlXhb9IktysvMYdHJFyw.jpeg',
            'apartments/IRW0hGA1DiuMLu2SJLf88YRV4vxfTRDEwNpFxSWN.jpeg',
            'apartments/KRDAmQU6Pyws4Oz96ZqyucvetD4XQpvNDKOr3cX2.jpeg',
            'apartments/OiaUrylDp4wNqrYaWjzA5sWIhw9HTWeWyLzTNnqy.jpeg',
            'apartments/oTazHVAGe8DqTWfun9qOhIhJpi6cmMHNnYjDzRAk.jpeg',
            'apartments/RAeFCGtDUGJPSFx5EVa0JIP2g6De4Ep5EwxwM7Zh.jpeg'
        ];

        $faker = Faker::create();
        foreach (range(1, 10) as $i) {

            if ($i === 1) {
                DB::table('users')->insert([
                    'name' => 'Mike',
                    'email' => 'michaszo18@gmail.com',
                    'password' => Hash::make('password'),
                    'role' => 'admin',
                    'phone_number' => $faker->tollFreePhoneNumber,
                ]);
            }

            DB::table('users')->insert([
                'name' => $faker->name,
                'email' => $faker->email,
                'password' => Hash::make('password'),
                'role' => 'owner',
                'phone_number' => $faker->tollFreePhoneNumber,
                'created_at' => $faker->dateTimeThisMonth($max = 'now', $timezone = null)
            ]);

            foreach (range(1, rand(1, 3)) as $j) {
                DB::table('apartments')->insert([
                    'title' => $faker->sentence(5),
                    'owner_id' => $i,
                    'city' => $cities[rand(0, count($cities) - 1)],
                    'street' => $faker->streetName,
                    'apartment_number' => $faker->buildingNumber,
                    'number_of_beds' => rand(1, 5),
                    'rental_price' => rand(1, 5) * 1000,
                    'fees' => rand(1, 5) * 100,
                    'description' => $faker->realText(150),
                    'img_1' => $imgs[rand(0, 7)],
                    'img_2' => $imgs[rand(0, 7)],
                    'img_3' => $imgs[rand(0, 7)],
                    'img_4' => $imgs[rand(0, 7)],
                    'created_at' => $faker->dateTimeThisMonth($max = 'now', $timezone = null)
                ]);
            }
        }

    }
}

