<?php

use Illuminate\Database\Seeder;
use Faker\Factory as Faker;

class RoomSeeder extends Seeder
{
    /**
     * Run the database seeds.
     *
     * @return void
     */
    public function run()
    {
        $faker = Faker::create();
        $apartments = \App\Apartment::all();

        $imgs = [
            'apartments/1SOMTTzqRamBsHNuYBOt6tdOHDLNV1oRzuqqdzWZ.jpeg',
            'apartments/2zouAVNw7HTamNtd4IeWzWdE56BtW6ILJlhLla61.jpeg',
            'apartments/Aey4PaXhLoWlR1NAvxvyXlXhb9IktysvMYdHJFyw.jpeg',
            'apartments/IRW0hGA1DiuMLu2SJLf88YRV4vxfTRDEwNpFxSWN.jpeg',
            'apartments/KRDAmQU6Pyws4Oz96ZqyucvetD4XQpvNDKOr3cX2.jpeg',
            'apartments/OiaUrylDp4wNqrYaWjzA5sWIhw9HTWeWyLzTNnqy.jpeg',
            'apartments/oTazHVAGe8DqTWfun9qOhIhJpi6cmMHNnYjDzRAk.jpeg',
            'apartments/RAeFCGtDUGJPSFx5EVa0JIP2g6De4Ep5EwxwM7Zh.jpeg'
        ];


        foreach ($apartments as $apartment) {
            $owner_id = $apartment->owner->id;

            DB::table('rooms')->insert([
                'title' => $faker->sentence(6),
                'owner_id' => $owner_id,
                'apartment_id' => $apartment->id,
                'number_of_beds' => 1,
                'rental_price' => rand(5, 10) * 100,
                'fees' => rand(1, 5) * 10,
                'description' => $faker->realText(50),
                'img' => $imgs[rand(0, 7)],
            ]);
        }
    }
}
