<?php

use Illuminate\Database\Seeder;
use Faker\Factory as Faker;

class RentSeeder extends Seeder
{
    /**
     * Run the database seeds.
     *
     * @return void
     */
    public function run()
    {
        $faker = Faker::create();
        $rooms = \App\Room::all();
        $tenants = \App\User::all()->where('role', 'tenant');

        $tenants_ids = [];
        foreach ($tenants as $tenant) {
            array_push($tenants_ids, $tenant->id);
        }

        foreach ($rooms as $room) {
            DB::table('rents')->insert([
                'owner_id' => $room->owner->id,
                'tenant_id' => array_shift($tenants_ids),
                'apartment_id' => $room->apartment->id,
                'room_id' => $room->id,
                'rental_price' => $room->apartment->rental_price,
                'fees' => $room->apartment->fees,
                'contract_to' => $faker->dateTimeBetween('+1 month', '+1 year'),
                'notes' => $faker->realText(150),
                'created_at' => $faker->dateTimeThisMonth($max = 'now', $timezone = null)
            ]);
        }
    }
}
