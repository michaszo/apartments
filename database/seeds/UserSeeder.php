<?php

use Illuminate\Database\Seeder;
use Faker\Factory as Faker;

class UserSeeder extends Seeder
{
    /**
     * Run the database seeds.
     *
     * @return void
     */
    public function run()
    {
        $faker = Faker::create();
        foreach (range(1, 20) as $index) {
            DB::table('users')->insert([
                'name' => $faker->name,
                'email' => $faker->email,
                'password' => Hash::make('password'),
                'role' => 'tenant',
                'phone_number' => $faker->tollFreePhoneNumber,
                'created_at' => $faker->dateTimeThisMonth($max = 'now', $timezone = null)
            ]);
        }
    }
}
