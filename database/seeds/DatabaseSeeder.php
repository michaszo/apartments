<?php

use Illuminate\Database\Seeder;

class DatabaseSeeder extends Seeder
{
    /**
     * Seed the application's database.
     *
     * @return void
     */
    public function run()
    {
         $this->call(ApartmentSeeder::class);
         $this->call(UserSeeder::class);
         $this->call(FinanceSeeder::class);
         $this->call(RoomSeeder::class);
         $this->call(RentSeeder::class);
    }
}
